// sessionをcartに反映させる
$(function(){
	reflectcart();
});

// カートに入れる
function addtocart(item_id, item_name, item_price){
    swal({
        title: item_name + "をカートに入れますか?",
	    type: "info",
	    showCancelButton: true,
	    confirmButtonColor: "#DD6B55",
	    confirmButtonText: "カートに入れる",
	    closeOnConfirm: false
    }, function(isConfirm){
        if(isConfirm){
	        swal("カートに入れました。", "", "success");
	        $.cookie.json = true;
	        // cartboxの初期化
	        var cartbox = initializeCartbox();
            
	        // cartboxにitemを入れる
	        if(cartbox.hasOwnProperty(item_id)){
	            cartbox[item_id]['freq'] += 1;
	        }else{
	            cartbox[item_id] = {'freq': 1, 'name': item_name, 'price': item_price};
	        }
	        $.cookie('cart', cartbox, { expires: 7, path: '/' });
	        reflectcart();
        }
    });
}

function resetcart(){
    swal({
        title: "カートの中身を空にしました",
        type: "success",
        timer: 1500,
        showConfirmButton: false
    });
    $.removeCookie('cart', {path: '/'});
    reflectcart();
}

function payment(){
    swal({
        title: "購入しますか",
	    text: "購入画面に進みます",
	    type: "warning",
	    showCancelButton: true,
	    confirmButtonColor: "#DD6B55",
	    confirmButtonText: "購入する",
	    closeOnConfirm: false
    }, function(isConfirm){
        if(isConfirm){
	  	    location.href = "http://makecolors.info/food/index.php/main/thanks";
        }
    });
}

// cartboxの初期化
function initializeCartbox(){
    var cartbox = {};
    if(typeof($.cookie('cart')) !== "undefined"){
	    cartbox = $.cookie('cart');
	    if(typeof(cartbox) === "string"){
	        cartbox = JSON.parse($.cookie('cart'));
	    }else{
	        cartbox = $.cookie('cart');
	    }
    }
    return cartbox;
}

// cartへの反映
function reflectcart(){
    var cartbox = initializeCartbox();
    var $cartbox_tbody = $('#cartbox > tbody');
    var total_price = 0;
    $cartbox_tbody.empty();
    for (var item in cartbox){
	    var $tr = $('<tr></tr>');
	    $tr.append('<td>' + cartbox[item].name + '</td>');
	    $tr.append('<td>' + cartbox[item].freq + '</td>');
	    $tr.append('<td>￥' + cartbox[item].price + '</td>');
	    $cartbox_tbody.append($tr);
	    total_price += Number(cartbox[item].price * cartbox[item].freq);
    }
    $('#total_price').text('￥' + total_price);
    
}
