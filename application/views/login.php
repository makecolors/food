<!DOCTYPE html>
<html lang="ja">
<head>
   <meta charset="utf-8">
   <title>ログイン画面 | Aoyama Canteen</title>
   <link href= "<?php echo base_url() . "lib/bootstrap-3.3.5-dist/css/bootstrap.min.css "?>" rel="stylesheet">
   <link href= "<?php echo base_url() . "css/main.css "?>" rel="stylesheet">
</head>

<body id="login_background">
<img id="login_chapel" src="<?php echo base_url() . "image/chapel.png"?>" alt="log2">
<div id="login_container">
   <div id="login_title" class="center-block">
     <h1 id="login_logo"><img src="<?php echo base_url() .  "image/aoyama.png"?>" alt="logo">Aoyama Canteen</h1>
   </div>
   <div id="login_form">
   <?php
 
   echo form_open("index.php/main/login_validation");
   echo validation_errors();//バリデーションのエラー表示用

   echo "<p>学籍番号:";
   echo form_input("customer_id", $this->input->post("customer_id"));
   echo "</p>";
 
   echo "<p>パスワード:";
   echo form_password("password");
   echo "</p>";
 
   echo "<p>";
   echo form_submit("login_submit", "Login");//ユーザー登録ボタンを出力
   echo "</p>";
 
   echo form_close();//フォームを閉じる
 
   ?>
   </div>
</div>
 
</body>
</html>