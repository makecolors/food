<?php 
  $session = $this->session->all_userdata();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
   <meta charset="utf-8">
   <title>食券選択 | Aoyama Canteen</title>
   <link href= "<?php echo base_url() . "lib/bootstrap-3.3.5-dist/css/bootstrap.min.css "?>" rel="stylesheet">
   <link href= "<?php echo base_url() . "css/main.css "?>" rel="stylesheet">
   <link href= "<?php echo base_url() . "lib/sweetalert-master/dist/sweetalert.css "?>" rel="stylesheet">
   <script src= "<?php echo base_url() . "lib/jquery-2.1.4.min.js "?>"></script>
   <script src= "<?php echo base_url() . "lib/sweetalert-master/dist/sweetalert.min.js "?>"></script>
   <script src= "<?php echo base_url() . "lib/jquery.cookie.js "?>"></script>
   <script src= "<?php echo base_url() . "js/cart.js "?>"></script>
</head>
<body>
 
<div class="container">
   
   <div class="row">
     <div class="col-md-10">
       <h1>Aoyama Canteen</h1>
       <p>食券販売機 | 混雑予想アプリ</p>
     </div>
     <div class="col-md-2">
       <p>学籍番号: <?php print_r($session['customer_id']);?></p>
       <p>残金: <?php print_r($session['balance']);?></p>
       <a href="<?php echo base_url() . "index.php/main/logout" ?>">ログアウト</a>
     </div>
   </div>
   <div class="row">
     <div id="menubar" class="col-md-3">
       <div id="menulink">
         <h3>メニュー</h3>
         <a href="<?php echo base_url() . "index.php/main/stats" ?>"           class="btn btn-danger btn-lg active btn-block"  role="button">混雑予想</a>
         <a href="<?php echo base_url() . "index.php/main/members/bowl" ?>"    class="btn btn-primary btn-lg active btn-block" role="button">丼</a>
         <a href="<?php echo base_url() . "index.php/main/members/setmeal" ?>" class="btn btn-primary btn-lg active btn-block" role="button">ランチ</a>
         <a href="<?php echo base_url() . "index.php/main/members/ramen" ?>"   class="btn btn-primary btn-lg active btn-block" role="button">ラーメン</a>
         <a href="<?php echo base_url() . "index.php/main/members/soba" ?>"    class="btn btn-primary btn-lg active btn-block" role="button">そば</a>
         <a href="<?php echo base_url() . "index.php/main/members/udon" ?>"    class="btn btn-primary btn-lg active btn-block" role="button">うどん</a>
         <a href="<?php echo base_url() . "index.php/main/members/others" ?>"    class="btn btn-primary btn-lg active btn-block" role="button">その他</a>
         <button class="btn btn-warning btn-lg active btn-block" onclick="resetcart()">リセット</button>
         <button class="btn btn-success btn-lg active btn-block" onclick="payment()">会計</button>
       </div>

       <table id="cartbox" class="table table-condensed">
         <thead><tr><th>商品名</th><th>個数</th><th>価格</th></tr></thead>
         <tbody></tbody>
         <tfoot><td colspan="2">合計</td><td id="total_price"></td></tfoot>
       </table>
     </div>
     <div id="menu" class="col-md-9">
        <div class="row">
          
          <?php
           foreach($record as $row){
              echo('<div id="' . $row->image_path . '" class="col-md-4 menuitem" onclick="addtocart(\''
                   . $row->item_id  . '\',\'' 
                   . $row->item_name  . '\',\''
                   . $row->price
                   . '\')"> ');
              echo('<img src="' . base_url() . '/image/' . $row->image_path . '" width="250px" height="200px">');
              echo($row->item_name);
              echo('￥' . $row->price);
	      echo('</div>');
           }
          ?>
        </div>
     </div>
   </div>
</div>

<script>
    $('#menu>div>div').each(function(){
	console.log($(this).attr('id'));
    });
</script>
 
</body>
</html>