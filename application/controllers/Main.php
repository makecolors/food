<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Main extends CI_Controller {

  // 最初に参照される関数
  public function index()
  {
      $this->login();
  }

  // ログイン時の動作
  public function login()
  {
    $this->load->view('login');
  }

  // ログアウト時の動作
  public function logout()
  {
    $this->session->sess_destroy();
    redirect("index.php/main/login");
  }

  // ログイン時のエラーチェック
  public function login_validation()
  {
    $this->load->library("form_validation");
 
    $this->form_validation->set_rules("customer_id", "メール", "required|trim|callback_validate_credentials");
    $this->form_validation->set_rules("password", "パスワード", "required|md5|trim");

    // バリデーションエラーのチェック
    if($this->form_validation->run()){
      $data = array(
	    "customer_id" => $this->input->post("customer_id"),
	    "is_logged_in" => 1
      );

      $this->session->set_userdata($data);
      redirect("index.php/main/stats");
    }else{
      $this->load->view("login");
    }
    // echo $_POST["email"];
  }

  // ログイン処理を通過した後の処理
  public function members($type)
  {
    // 正規処理をした場合はページを見せ、不正アクセスをしている場合はリダイレクト
    if($this->session->userdata("is_logged_in")){
      
      $this->db->where("type", $type);
      $query = $this->db->get("item");
      $data['record'] = $query->result();

      $this->load->view("members", $data);
    }else{
      redirect("index.php/main/restricted");
    }
  }
  
  public function thanks(){
    $this->load->helper('cookie');
    $this->load->helper('string');

    $session = $this->session->all_userdata();
    $data['order_id'] = random_string('alnum', 16);
    $cookie_cart = json_decode(get_cookie('cart'));
    $total_price = 0;
    
    // 購入履歴をpurchase_historyテーブルに挿入する
    foreach ($cookie_cart as $key => $value){
      $data = array(
	  'order_id' => $data['order_id'],
	  'customer_id' => $session['customer_id'],
	  'item_id' => $key,
	  'item_num' => $value->freq
      );
      $total_price += $value->price * $value->freq;
      //var_dump($data);
      //print_r('<br>');
      $this->db->insert('purchase_history', $data);
    }

    // 購入した代金をユーザの持ち金から差し引く
    //var_dump($session['balance']);
    $session['balance'] -= $total_price;
    $userdata = array(
	  'balance' => $session['balance']
    );
    $this->db->where('customer_id', $session['customer_id']);
    $this->db->update('customer', $userdata);
    $this->session->set_userdata($session);
    
    // cookieの削除
    delete_cookie('cart');

    $this->load->view('thanks', $data);
  }

  // 混雑度予想を表示する
  public function stats(){
      // SQL: SELECT DISTINCT type FROM item
      $this->db->select('type');
      $this->db->distinct();
      $query_type = $this->db->get('item');
      //var_dump($query_type);
      date_default_timezone_set('Asia/Tokyo');
      $time_st = date( "Y-m-d 00:00:00", time() );
      $time_end = date( "Y-m-d H:i:s", time() );

      foreach($query_type->result() as $row){
          // 購買記録の食券交換前と交換後の人数を取得する
          $this->db->from('purchase_history');
          $this->db->select('purchase_history.order_id, purchase_history.received_check');
          //$this->db->distinct();
          $this->db->join('item', 'purchase_history.item_id = item.item_id', 'inner');
          $this->db->where("purchase_history.date BETWEEN '${time_st}' AND '${time_end}'");
          $this->db->where('item.type', $row->type);
          $get_item_result = $this->db->get();
          //echo $this->db->get_compiled_select();
          //var_dump(count($get_item_result->result()));
          $data['record']["$row->type"]['count'] = count($get_item_result->result());

          $data['record']["$row->type"]['received_count'] = 0;
          foreach($get_item_result->result() as $check){
              // var_dump($check);
              if($check->received_check == true){
                  $data['record']["$row->type"]['received_count'] += 1;
              }
          }
          
      }
      //var_dump($data);
      $this->load->view('stats', $data);
  }

  // 不正アクセスの場合の処理
  public function restricted()
  {
    $this->load->view("restricted");
  }

  // Email情報がPOSTされたときに呼び出されるコールバック機能
  public function validate_credentials()
  {
    $this->load->model("model_users");
    // ユーザがログインに失敗したときは、エラー表示をする
    if($this->model_users->can_log_in()){
      return true;
    }else{
      $this->form_validation->set_message("validate_credentials", "ユーザー名かパスワードが異なります. ");
      return false;
    }
  }
  
}