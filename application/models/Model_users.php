<?php
 
class Model_users extends CI_Model{

  // ログイン照合処理
  public function can_log_in(){
 
    $this->db->where("customer_id", $this->input->post("customer_id"));
    $this->db->where("password", md5($this->input->post("password")));
    $query = $this->db->get("customer");
    $user_record = $query->result();
 
    // ユーザの存在チェック
    if($query->num_rows() == 1){
      foreach($user_record as $row){
        $this->session->set_userdata(array('balance' => $row->balance));
      }
      return true;
    }else{
      return false;
    }
  }
}