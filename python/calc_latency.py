#! /usr/local/python/bin/python
# -*- coding: utf-8 -*-
'''
混合ガウス分布から待ち時間を推定する
input: 人数, 時間
output: 画像ファイルと現在の待ち時間
'''
import numpy as np
import matplotlib
matplotlib.use('Agg') # CUI上で画像ファイルを生成するのに必要

import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import scipy.stats.distributions as dis
import matplotlib.pylab as pylab
from matplotlib.font_manager import FontProperties
import mysql.connector
import datetime
import json

def get_usernum():
  # データベースの接続設定
  con = mysql.connector.connect(
    host='localhost',
    db='food',
    user='root',
    passwd='7rmwmhs6tr',
    buffered=True
  )
  
  # データベースからtypeを取り出す
  cur = con.cursor()
  cur.execute('SELECT DISTINCT item.type FROM item')
  types = cur.fetchall()
  
  # 購入人数を取り出す
  time_st = datetime.datetime.now().strftime("%Y-%m-%d 00:00:00")
  time_ed = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
  item_waittime = {}
  for itemtype in types:
    query = "SELECT COUNT(order_id) FROM purchase_history INNER JOIN item ON purchase_history.item_id = item.item_id WHERE date BETWEEN '" + time_st + "' AND '" + time_ed + "' AND type = '" + itemtype[0]  +"';"
    cur.execute(query)
    rows = cur.fetchall()
    item_waittime[itemtype[0]] = rows[0][0]
    
  cur.close()
  con.close()
  return item_waittime

"""
現在の時間を営業時間(-240~240)の範囲に変換する
"""
def projection(minute):
  if(minute < 600): # 10:00以前の場合
    return -240
  elif(1080 < minute): # 18:00以降の場合
    return 240
  else:
    return minute - 600 - 240
    
  
# 各待ち人数を取得する
item_waittime = get_usernum()
#print("各待ち人数の情報: ")
#print(item_waittime)

# 初期設定(input)
cur_time = -85
customer_num = 1000
wait_users_num = 0
file_path = "/var/www/html/food/python/"

# ガウス分布の初期設定
gauss_d_num = 4
gaussmix_pd = 0
normset = []
locset = (-210, -90, 50, 155)
scaleset = (30, 20, 20, 20)
pre_time = -85
post_time = -80

# グラフのクリア
plt.clf();
plt.cla();

# -240(10:00)~240(18:00)、中心0を14:00とする.
x = np.linspace(-240, 240, customer_num) #1000分割した要素を作成する
pi_k = np.array([0.1, 0.7, 0.1, 0.1]) # 混合ガウス分布の比率

for i in range(gauss_d_num):
  # ガウス分布の作成
  normset.append(dis.norm.pdf(x, loc = locset[i], scale = scaleset[i]))
  # ガウス分布の描画
  plt.plot(x, pi_k[i] * normset[i], color="blue")
  # ガウス分布の合成 (混合ガウス分布の作成)
  gaussmix_pd += pi_k[i] * normset[i]

# グラフの書式設定
fp = FontProperties(fname='/usr/share/fonts/ipa-mincho/ipam.ttf')
fig = plt.figure(figsize=(10, 3))
fig.patch.set_alpha(0.0)
plt.title(u'食堂全体の混雑予想度マップ', fontdict={'fontproperties':fp}, fontsize=24)
plt.xlabel(u'時刻', fontdict={'fontproperties':fp}, fontsize=16)
plt.ylabel(u'推定待ち時間(秒)', fontdict={'fontproperties':fp}, fontsize=16)
plt.xticks([-210, -90, 50, 155], ["10:30", "12:30", "14:50", "16:35"])


# 食券購入者の合計を計算する
sum = 0
for value in item_waittime.values():
  sum += value

#print("食券購入者人数: ")
#print(sum)

# 確率密度*総利用ユーザ数*一人当たりの処理時間(秒),で待ち時間を算出する
plt.plot(x, gaussmix_pd * sum * 30, color="red")
# plt.plot(x, gaussmix_pd * len(x) * 0.5, color="red")
# plt.plot(x, gaussmix_pd, color="red")

# 現在時間を求める
now = datetime.datetime.now()
dayseconds = (now - datetime.datetime(now.year, now.month, now.day)).seconds
proj_time = projection(dayseconds/60)
#print("-240~240の範囲内に変換した時間: ")
#print(proj_time)

# 積分して確率を求める (台形面積の総和)
# quadrange = np.searchsorted(x, [proj_time - 5, proj_time])
quadrange = np.searchsorted(x, [-90, -85])
prob = 0;
for d in xrange(quadrange[0], quadrange[1]):
  if(d == quadrange[1]):
    break
  prob += (gaussmix_pd[d] + gaussmix_pd[d+1]) * (x[d+1] - x[d]) / 2
#print("ある時間における確率: ")
#print(prob)

# 待ち人数を変換し、待ち時間を保存
for key, value in item_waittime.items():
  item_waittime[key] = prob * value * 30

# jsonファイルに待ち時間を書き出す
with open(file_path + 'calc_latency.json', 'w') as f:
  json.dump(item_waittime, f, sort_keys=True, indent=4)

# 画像ファイルの保存
pylab.tight_layout() # グラフが見切れないようにする
pylab.savefig(file_path + 'calc_latency.svg')



