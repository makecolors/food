#! /usr/local/python/bin/python
# -*- coding: utf-8 -*-
'''
混合ガウス分布から待ち時間を推定する
input: 人数, 時間
output: 画像ファイルと現在の待ち時間
'''
import numpy as np
import matplotlib
matplotlib.use('Agg') # CUI上で画像ファイルを生成するのに必要

import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import scipy.stats.distributions as dis
import matplotlib.pylab as pylab
from matplotlib.font_manager import FontProperties

#初期設定
cur_time = -85
customer_num = 1000

# グラフのクリアと設定
plt.clf();
plt.cla();
pylab.figure(figsize=(10, 3))

# -240(10:00)~240(18:00)、中心0を14:00とする.
x = np.linspace(-240, 240, customer_num) #1000分割した要素を作成する
pi_k = np.array([0.1, 0.7, 0.1, 0.1]) # 混合ガウス分布の比率

# 各ガウス分布の生成
norm0 = dis.norm.pdf(x, loc = -210, scale = 30)
norm1 = dis.norm.pdf(x, loc =  -90, scale = 20)
norm2 = dis.norm.pdf(x, loc =   50, scale = 20)
norm3 = dis.norm.pdf(x, loc =  155, scale = 20)

plt.plot(x, pi_k[0] * norm0, color="blue")
plt.plot(x, pi_k[1] * norm1, color="blue")
plt.plot(x, pi_k[2] * norm2, color="blue")
plt.plot(x, pi_k[3] * norm3, color="blue")

# グラフの書式設定
fp = FontProperties(fname='/usr/share/fonts/ipa-mincho/ipam.ttf')
plt.title(u'食堂の推定待ち時間', fontdict={'fontproperties':fp})
plt.xlabel(u'時間', fontdict={'fontproperties':fp})
plt.ylabel(u'推定待ち時間(分)', fontdict={'fontproperties':fp})

# 確率密度*総利用ユーザ数*一人当たりの処理時間,で待ち時間を算出する
plt.plot(x, (pi_k[0] * norm0 + pi_k[1] * norm1 + pi_k[2] * norm2 + pi_k[3] * norm3) * len(x) * 0.5, color="red")

# 軸ラベルを設定する
plt.xticks([-210, -90, 50, 155], ["10:30", "12:30", "14:50", "16:35"])

# (正確)与えられた時間から5分前までの人数を求める
wait_users_num = x[np.where(((cur_time-10)<=x)&(x<=cur_time))]
print len(wait_users_num)
print len(x)
print float(len(wait_users_num)) / len(x)

# 画像ファイルの保存
pylab.tight_layout() # グラフが見切れないようにする
pylab.savefig('waitminute.svg')
